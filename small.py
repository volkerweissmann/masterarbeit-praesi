from common import *


class Small(MyScene):
    def construct(self):
        off = np.array([0, 3.2, 0])
        delt = np.array([0, -1.5, 0])
        self.add(
            MathTex(
                r"\partial_t\mathbf{q}(t) = \mathbf F(\mathbf q(t)) + \sqrt{\epsilon}\underline{\underline C}(\mathbf q(t))\boldsymbol\zeta(t)"
            ).move_to(off),
            MathTex(
                r"\langle\zeta_i(t)\zeta_j(t')\rangle = \delta_{ij}\delta(t-t')\quad\forall i,j"
            ).move_to(off + delt),
            MathTex(
                r"2\underline{\underline Q}}(\mathbf q)) = \underline{\underline C}}(\mathbf q))\underline{\underline C}}(\mathbf q))^T"
            ).move_to(off + delt * 2),
            MathTex(
                r"\partial_t p(\mathbf q,t) = \hat{\mathcal L}p(\mathbf q,t)"
            ).move_to(off + delt * 3),
            MathTex(
                r"\hat{\mathcal L}p = -\sum_i \partial q_i\left(F_i p\right) + \epsilon \sum_i\sum_j \partial q_i\partial q_j\left(Q_{ij}p\right)"
            ).move_to(off + delt * 4),
        )
        self.wait()
