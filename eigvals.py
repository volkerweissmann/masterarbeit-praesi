from common import *


class Eigvals(MyScene):
    def construct(self):
        # langeND = MathTex(
        #     r"\partial_t \mathbf q(t) = \mathbf F(\mathbf q(t)) + \sqrt{\epsilon} \underline{\underline{C}}(\mathbf q(t)) \boldsymbol\zeta(t)"
        # )
        left_edge = -6
        pos1 = np.array([left_edge, 0, 0])
        pos2 = np.array([left_edge, -1, 0])
        fpte = MathTex(
            r"\partial_t p(\mathbf q, t) = ", "\hat{\mathcal L}", " p (\mathbf q, t)"
        ).move_to(np.array([left_edge, 2, 0]), aligned_edge=LEFT)
        fpsol = MathTex(
            r"p (\mathbf q, t|\mathbf q_0) = e^{\hat{\mathcal L}t}\delta(\mathbf q-\mathbf q_0)",
        ).move_to(pos1, aligned_edge=LEFT)

        self.subtitle(
            "Nun wollen ich formal beweisen, dass wir im allgemeinen gedämpfte Oszillationen auftreten. Wir schreiben die Fokker-Planck Gleichung so. Die formale Lösung für diese DGL ist das hier, wenn das System bei q_0 startet."
        )
        self.play(Write(fpte))

        self.next_section()
        arrow1 = Arrow(
            np.array([left_edge + 2, 1.7, 0]), np.array([left_edge + 2, 0.3, 0])
        )
        self.play(Create(arrow1))
        self.play(Write(fpsol))

        self.next_section()
        eve = MathTex(
            r"\lambda_k v_k(\mathbf q) = ", "\hat{\mathcal L}", "v_k(\mathbf q)"
        ).move_to(np.array([left_edge, 3, 0]), aligned_edge=LEFT)
        self.subtitle("Wenn wir jetzt den Fokker-Planck Operator nehmen.")
        self.play(
            TransformFromCopy(fpte[1], eve[1])
        )  # todo: die transformation ist nicht ganz so geil
        self.subtitle("Und die Eigenwerte und Eigenfunktionen finden.")
        self.play(Write(eve[0]))
        self.play(Write(eve[2]))

        self.next_section()
        self.subtitle("Können wir unsere formale Lösung so schreiben.")
        spectral = MathTex(
            r"p (\mathbf q, t|\mathbf q_0) =",
            r"\sum_k e^{\lambda_k t} c_k(\mathbf q_0) v_k(\mathbf q)",
        ).move_to(pos1, aligned_edge=LEFT)
        self.play(TransformMatchingShapes(fpsol, spectral))
        self.next_section()

        # lfgr = MathTex(
        #     r"\left\langle f(\mathbf q(t)) g(\mathbf q(0))\right\rangle"
        # ).move_to(pos2, aligned_edge=LEFT)
        lfgr = MathTex(
            r"&\langle",
            r"f",
            r"(\mathbf q(t))",
            r"g",
            r"(\mathbf q(0))",
            r"\rangle",
            r"\\=&\int\mathrm d\mathbf q_0\int\mathrm d\mathbf q_1 f(\mathbf q_1)g(\mathbf q_0)",
            r"p^0(\mathbf q_0)",
            r"p(\mathbf q_1, t|\mathbf q_0)",
        ).move_to(pos2, aligned_edge=UL)
        self.subtitle(
            "Die allgemeinste Funktion, die gedämpfte Oszillationen zeigt, ist die hier."
        )
        self.play(Write(lfgr[0:6]))
        self.subtitle("Das ist der Ensemble-Mittelwert")
        self.secind([lfgr[0], lfgr[5]])
        self.subtitle("einer beliebigen Observablen f")
        self.secind([lfgr[1]])
        self.subtitle("die vom Zustand zur Zeit t abhängig ist")
        self.secind([lfgr[2]])
        self.subtitle("mal eine andere Observable g")
        self.secind([lfgr[3]])
        self.subtitle("die vom Anfangszustand abhängig ist.")
        self.secind([lfgr[4]])
        self.subtitle(
            "Wer in ASP aufgepasst hat, der weiß dass das gerade dieses Integral hier ist."
        )
        self.play(Write(lfgr[6:9]))
        self.next_section()
        self.subtitle("p^0 ist die Verteilung aus der der Anfangszustand gezogen wird.")
        self.secind([lfgr[7]])
        self.secind([spectral, lfgr[8]])
        self.subtitle(
            "Wir können jetzt die Lösung der Fokker-Planck Gleichung hier einsetzen."
        )

        lfgr2 = MathTex(
            r"&\langle",
            r"f",
            r"(\mathbf q(t))",
            r"g",
            r"(\mathbf q(0))",
            r"\rangle",
            r"\\=&\int\mathrm d\mathbf q_0\int\mathrm d\mathbf q_1 f(\mathbf q_1)g(\mathbf q_0)",
            r"p^0(\mathbf q_0)",
            r"\sum_k e^{\lambda_k t} c_k(\mathbf q_0) v_k(\mathbf q_1)",
        ).move_to(pos2, aligned_edge=UL)
        self.play(FadeOut(lfgr[8]), TransformFromCopy(spectral[1], lfgr2[8]))
        self.next_section()

        lfgr3 = MathTex(
            r"&{{\langle f(\mathbf q(t)) g(\mathbf q(0)) \rangle}} \\{{=}}&{{\int\mathrm d\mathbf q_0\int\mathrm d\mathbf q_1 f(\mathbf q_1)g(\mathbf q_0) p^0(\mathbf q_0)}} {{\sum_k e^{\lambda_k t}}} {{c_k(\mathbf q_0) v_k(\mathbf q_1)}}",
        ).move_to(pos2, aligned_edge=UL)
        lfgr4 = MathTex(
            r"&{{\langle f(\mathbf q(t)) g(\mathbf q(0)) \rangle}} \\{{=}}&{{\sum_k e^{\lambda_k t}}} {{\int\mathrm d\mathbf q_0\int\mathrm d\mathbf q_1 f(\mathbf q_1)g(\mathbf q_0) p^0(\mathbf q_0)}} {{c_k(\mathbf q_0) v_k(\mathbf q_1)}}",
        ).move_to(pos2, aligned_edge=UL)

        self.remove(*lfgr, *lfgr2)
        self.add(lfgr3)
        self.subtitle("Dann stellen wir die Reihenfolge um")
        self.play(TransformMatchingTex(lfgr3, lfgr4, run_time=3))
        self.next_section()
        self.subtitle("Und nennen den hinteren Teil a_k")
        lfgr5 = MathTex(
            r"&{{\langle f(\mathbf q(t)) g(\mathbf q(0)) \rangle}} \\{{=}}&{{\sum_k e^{\lambda_k t}}} {{a_k}}",
        ).move_to(pos2, aligned_edge=UL)
        self.play(TransformMatchingTex(lfgr4, lfgr5))
        self.wait()
