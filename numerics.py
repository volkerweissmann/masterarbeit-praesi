from common import *


class Numerics(MyScene):
    def construct(self):
        self.add(
            MathTex(
                r"\langle",
                r"f\left(",
                r"q(t)",
                r"\right)",
                r"g(q(0))",
                r"\rangle",
                r"= \sin(\omega t) e^{-\gamma t}",
                r"+ a_0 + h(t)",
            ).move_to([0, 2, 0])
        )
        self.add(
            MathTex(
                r"R=\frac{\omega}{\gamma}",
            )
        )
        self.wait()
