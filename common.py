import sys
from manim import *
from enum import Enum
import scipy.integrate
import textwrap

SUBTITLES_WRITE_OUT = False
SUBTITLES_ON = False
LONG_WAITS = True
FRAMEDT = 1 / 60


class MyScene(Scene):
    def __init__(self):
        super(MyScene, self).__init__()
        self.current_subtitle = None

    def subtitle(self, text):
        if SUBTITLES_WRITE_OUT:
            with open("sub.txt", "a") as ofile:
                ofile.write(text + "\n")
        if SUBTITLES_ON:
            if self.current_subtitle is not None:
                self.remove(self.current_subtitle)
            self.current_subtitle = Text(
                "\n".join(textwrap.wrap(text, 130)), font_size=15
            ).move_to([0, -3.5, 0])
            self.add(self.current_subtitle)

    def secind(self, mobs, name="unnamed"):
        self.play(
            *[
                Indicate(mob, scale_factor=1.2, color=YELLOW, rate_func=smooth)
                for mob in mobs
            ]
        )
        self.next_section(name)
        self.play(
            *[
                Indicate(mob, scale_factor=1 / 1.2, color=WHITE, rate_func=smooth)
                for mob in mobs
            ]
        )


class Trans:
    def __init__(self):
        self.perline = Line([-6, 0, 0], [6, 0, 0])
        self.rect = Rectangle(width=12, height=3, color=BLUE, fill_opacity=0.5).move_to(
            [0, 0, 0]
        )


AXS_RAD = 2.3
