from common import *


class Cosexp(MyScene):
    def construct(self):
        ax = Axes(
            x_range=[0, 40, 1],
            y_range=[-1, 1, 1],
            x_length=10,
            axis_config={"include_ticks": False},
            # axis_config={"color": GREEN},
            # x_axis_config={
            #     "numbers_to_include": np.arange(-10, 10.01, 2),
            #     "numbers_with_elongated_ticks": np.arange(-10, 10.01, 2),
            # },
            tips=False,
        )
        labels = ax.get_axis_labels(x_label="t", y_label="")
        eqn1 = MathTex(
            r"\langle y\rangle =",
            r"\langle",
            r"\sin\left(",
            r"q(t)",
            r"\right)",
            r"\rangle",
            r"= \sin(\omega t) e^{-\gamma t}",
        )
        eqn1.move_to(np.array([0, 3, 0]))
        ylabel = MathTex(
            r"\langle y\rangle =\langle \sin(q) \rangle"
        )  # todo: y= sin(q) erklären
        ylabel.rotate(PI / 2)
        ylabel.move_to(np.array([-6, 0, 0]))
        gamma = 0.1
        sinexp = lambda t: np.sin(t) * np.exp(-gamma * t)
        graph = ax.plot(sinexp)
        self.subtitle(
            "Wenn man nicht über 20 Systeme, sondern über unendlich viele Systeme mittelt, erhält man eine Sinusfunktion mal eine Exponentialfunktion."
        )
        self.play(Create(ax), Write(labels), Write(ylabel))
        self.play(Create(graph), Write(eqn1))

        self.next_section()

        self.subtitle(
            "Die Periodendauer ist konstant und die Peaks liegen auf einer Exponentialfunktion."
        )
        crosses = []
        for i in range(6):
            t = 2 * np.pi * i + np.pi / 2
            pos = ax.coords_to_point(t, sinexp(t))
            crosses.append(Cross(scale_factor=0.08).move_to(pos))
        for cross in crosses:
            self.play(Create(cross), run_time=0.3)
        fit = ax.plot(lambda t: np.exp(-gamma * t), color=BLUE)
        self.play(Create(fit))

        self.next_section()
        self.play(
            FadeOut(fit),
            *map(FadeOut, crosses),
            FadeOut(graph),
            FadeOut(ax),
            FadeOut(labels),
            FadeOut(ylabel),
        )
        self.next_section()

        eqn2 = MathTex(
            r"\langle",
            r"f\left(",
            r"q(t)",
            r"\right)",
            r"g(q(0))",
            r"\rangle",
            r"= \sin(\omega t) e^{-\gamma t}",
            r"+ a_0 + h(t)",
        )
        eqn2.move_to(np.array([0, 1, 0]))

        self.subtitle(
            "Später werden wir sehen, dass in einem allgemeineren Fall, es ebenfalls eine Sinusfunktion mal eine Exponentialfunktion ist, also gedämpfte Oszillationen auftreten."
        )
        self.play(
            TransformFromCopy(eqn1[1], eqn2[0]),
            TransformFromCopy(eqn1[2], eqn2[1]),
            TransformFromCopy(eqn1[3], eqn2[2]),
            TransformFromCopy(eqn1[4], eqn2[3]),
            TransformFromCopy(eqn1[5], eqn2[5]),
            TransformFromCopy(eqn1[6], eqn2[6]),
            Write(eqn2[4]),
        )

        self.next_section()
        self.subtitle(
            "Im Allgemeinen gibt es noch einen weiteren Term h(t), der fällt aber schneller abfällt ab als der erste Term. Wir interessieren uns nur für das Verhalten für große Zeiten und vernachlässigen h(t) deshalb."
        )
        self.play(Write(eqn2[7]))

        self.next_section()
        self.subtitle(
            "Das Verhältnis aus der Kreisfrequenz und Dämpfungskonstante nennen wir die Robustness R und ist die Anzahl an Oszillationen nach denen die Höhe der Peaks um Faktor 535 abgefallen ist. Das ist auch der Gütefaktor der Oszillation."
        )
        eqnRdef = MathTex(r"\frac{\omega}{\gamma}", r"\equiv R")
        eqnRdef.move_to(np.array([0, -1, 0]))
        self.play(Write(eqnRdef[0]))

        self.next_section("second last")
        self.play(Write(eqnRdef[1]))

        self.next_section("last")
        self.wait()
