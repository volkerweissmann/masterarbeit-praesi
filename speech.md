Herzlich Willkommen zu meinem Masterarbeits-Vortrag. Ihr kennt mich glaube ich alle, wer mich nicht kennt, ich bin Volker Weißmann. Der offizielle Titel meiner Masterarbeit lautet "Gütefaktor von Oszillationen in der Langevin-Dynamik" und genau darum geht es in dieser Präsentation.

Ich habe mich mit Systemen die der überdämpften Langevin-Dynamik folgen beschäftigt. Da wir hier am ipt2 sind, will ich jetzt nicht erklären, was die Langevin-Gleichung oder die Fokker-Planck Gleichung ist, das wisst ihr alle. Es gibt aber unterschiedliche Konventionen, was wie definiert ist, also sind hier die Definitionen der Langevin-Gleichung, Fokker-Planck Gleichung und des Fokker-Planck Operators wie ich es definiert habe. Man beachte, dass wir ein epsilon haben, das ist ein Skalar den wir verwenden können um den Noise-Term zu skalieren. Das ist wichtig, denn wir betrachten später den weak-noise limit, also den Grenzwert von kleinen epsilons.

Im ersten Abschnitt will ich euch verständlich machen, wie es sein kann, dass manche Langevin-Systeme gedämpfte Oszillationen in Ensemble-Mittelwerten zeigen.

Dazu betrachten wir das einfachstes Beispielsystem. Einen Kreis.

Um genau zu sein, einen Punkt auf einem Kreis.

Und dieser Punkt bewegt sich mit konstanter Geschwindigkeit F_0 entlang des Kreises. Der Zustand q ist der Winkel.

Wenn wir uns jetzt die y-Koordinate als Funktion der Zeit anschauen, also den Sinus von q von t, dann sehen wir, dass das eine Sinusfunktion ist. Der entscheidende Punkt ist aber nicht, dass das eine Sinus-Funktion ist, sondern dass alle Peaks gleich hoch sind und alle Periodendauern gleich sind. Die Peaks liegen alle exakt auf den beiden dünnen weißen linien und sind in diesem Fall hier gerade 4 Sekunden voneinander entfernt.

Die Situation ändert sich allerdings wenn wir zu der konstanten Rate F_0 auch noch einen Noise-Term hinzufügen. Die Peaks sind jetzt immernoch alle gleich hoch, aber unterschiedlich weit voneinander entfernt. Die mittlere Periodendauer ist weiterhin 4 Sekunden, aber die Varianz ist nicht mehr null. Ihr seht hoffentlich das die Dynamik hier die Langevin Gleichung ist.

Im nächsten Schritt, simulieren wir jetzt nicht mehr ein einzelnes Langevin-System, sondern ein Ensemble aus 20 identischen und unabhängigen System.
Die orangene Linie ist der Ensemblemittelwert der y-Koordinate. Dadurch, dass wir über 20 Systeme mitteln wird das Rauschen schwächer und folgich die Abstände der Peaks gleichmäßiger. Wenn wir über unendlich viele System mitteln würden, wären alle Periodendauern wieder exakt 4 Sekunden. Der zweite Unterschied ist, dass nun die Peaks mit der Zeit abfallen, sie erreichen nicht mehr die weiße linie. Das liegt daran, dass sich die Systeme, also die gelben Punkte über den Kreis verteilen und der Schwerpunkt liegt dann eher im Zentrum als am Rand. Wichtig ist, dass ihr das versteht, weil das ist wichtig. Wir sehen hier ein Langevin-System bei dem der Ensemblemittelwert einer Observable eine gedämpfte Oszillation ausführt.

4:30

In diesem zweiten Abschnitt will ich erklären wie genau die Robustness, auch Gütefaktor genannt definiert ist.

Wenn man die selbe Simulation wie im letzen Abschnitt macht, aber nicht über 20 Systeme sondern über unendlich viele Systeme mittelt, erhält man das hier: Der Ensemble-Mittelwert der y-Koordinate ist eine Sinusfunktion mal eine Exponentialfunktion, also eine gedämpfte Oszillation. Da wir nun über unendlich viele Systeme mitteln gibt es kein Rauschen in dieser Kurve mehr.

Die Peaks haben alle den selben Abstand und liegen auf einer Exponentialfunktion.

<!-- In meiner Masterarbeit habe ich auch einen analytischen Beweis für dieses Ergebnis hier, aber der ist relativ langweilig, deshalb zeige ich den hier nicht. -->
Dieses Egebnis hier ist ein sehr spezifisches Ergebnis. Das gilt nur für Langevin-Systeme, die eindimensional sind, eine konstante treibende Kraft und konstante Diffusion haben und nur für die Observable sin q. Das kann allerdings verallgemeinert werden.

In einem Langevin-System mit beliebiger Dimension, treibender Kraft und Diffusion findet man eine gedämpfte Oszillation, wenn man diesen Ensemble-Mittelwert hier betrachtet. f und g sind hierbei zwei beliebige Observablen und das ganze funktioniert auch für beliebige Wahrscheinlichkeitsverteilungen aus denen man q(0) zieht.

Im Allgemeinen gibt es noch einen konstanten Term a_0 und einen weiteren Term h(t), der fällt aber schneller abfällt ab als der erste Term. Wir interessieren uns nur für das Verhalten für große Zeiten und vernachlässigen h(t) deshalb. Außerdem stimmt das nicht so ganz wenn ich sage, dass es für beliebige Langevin-Systeme mit beliebigen Observablen gibt. Man kann auch Systeme konstruieren, bei denen das nicht der Fall ist. Ein sehr triviales Beispiel wäre zum Beispiel die Observable als 0 zu definieren. Das kann man machen, dann hat man natürlich keine oszillation. Aber dieses Gesetz hier gilt zumindestens für sehr viele Systeme und viele Observablen. Für welche Systeme und Observablen genau es gilt habe ich nicht mir nicht viel angeschaut.

Das Verhältnis aus der Kreisfrequenz der Oszillation omega und Dämpfungskonstante gamma

nennen wir die Robustness R oder auch Gütefaktor. Das ist die Anzahl an Oszillationen nach denen die Höhe der Peaks um Faktor 535, das ist e^2pi, abgefallen ist. Unsere "research question" war es Methoden zu entwickeln um diese Robustness für verschiedene Systeme zu berechnen, oder nach oben oder unten abzugrenzen. Nachdem wir jetzt postuliert haben, dass es diese gedämpften Oszilalationen gibt,

8:20

wollen wir das jetzt auch formal beweisen. Dieser Abschnitt meiner Präsentation trägt den wunderschönen Namen "Von Eigenwerten zu Oszillationen", denn wir werden einen Zusammenhang zwischen den Eigenwerten des Fokker-Planck Operators und dem omega gamma und R der letzten Folie finden.

Das ist die Fokker-Planck Gleichung, L ist unser Fokker-Planck Operator.

Wir können direkt die formale Lösung hinschreiben. Die Wahrscheinlicheitsdichte zur Zeit t den Zustand q zu haben ist das hier, wenn das Teilchen bei q_0 startet. Jetzt ist e^Lt mal eine deltafunktion eher unpraktisch zu berechnen, also nehmen wir den Fokker-Planck Operator

und finden dessen Eigenwerte lambda_k und Eigenfunktionen v_k. Das ist die Eigenwertgleichung wie ihr hoffentlich wisst.

Dann können wir die formale Lösung so schreiben. Wer weiß an dieser Stelle schon woher die gedämpften Oszillationen kommen? ...

Das hier ist die Größe für die wir gedämpfte Oszilaltionen postuliert haben.

Das ist der Ensemble-Mittelwert

einer beliebigen Observablen f

die vom Zustand zur Zeit t abhängig ist

mal eine andere Observable g

die vom Anfangszustand abhängig ist.

Wer in ASP aufgepasst hat, der weiß dass man dieses Integral benutzen kann um das zu berechnen.

p^0 ist die Wahrscheinlichkeitsverteilung aus der der Anfangszustand q_0 gezogen wird.

Was dieser Term ist, das wissen wir von unserer formalen Lösung, also können wir das

hier einsetzen. Wir stellen die Reihenfolge der Terme um...

Den hinteren Teil fassen wir unter der Bezeichnung a_k zusammen

Jetzt wissen wir das hier. Und müssen das irgendwie weiter vereinfachen.

Dafür schauen wir uns die Stuktur der Eigenwerte lambda_k an. Publikumsfrage: Wie sind die Eigenwerte in der komplexen Ebene verteilt?
Man kann zeigen, dass es immer mindestens einen Eigenwert gibt, der 0 ist. Dieser Eigenwert gehört zum stationären Zustand. Wenn der Graph der diskretisierten Form des Fokker-Planck Operators "strongly connected" ist, gibt es genau Eigenwert der 0 ist. Das sollte mehr oder weniger immer der Fall sein.

Diesen Eigenwert nenn wir lambda_0. Man kann außerdem zeigen, dass alle anderen Eigenwerte einen negativen Realteil haben und entweder reell sind oder in komplex konjugierten Paaren vorkommen.

Wir können das nicht beweisen, aber wir nehmen an, dass es ein komplex konjugiertes Paar aus Eigenwerten gibt, das wir lambda_+1 und lambda_-1 nennen und alle anderen Eigenwerte haben einen kleineren Realteil, liegen also unterhalb der grünen Linie im grün schraffierten Bereich. Wir können zwar nicht beweisen dass es so ist, und es gibt auch Systeme bei denen das nicht so ist, aber dann haben wir ein Problem also nehmen wir an das das so aussieht. Wenn wir jetzt das lambda in Realteil und Imaginärteil aufspalten...

dann sieht man, dass für große Zeiten nur die Summanden mit dem größten Realteil entscheidend sind. Mit den Informationen aus dem rechten Diagramm, können wir es also

so schreiben. Die Summanden die wir vernachlässigen, da sie schneller abfallen habe ich als h(t) zusammengefasst. Das funktioniert nur, wenn a_+1 und a_-1 nicht verschwinden. Wir können zwar nicht beweisen dass sie nicht verschwinden, und es gibt auch Systeme bei denen sie verschwinden, aber wir nehmen wieder einfach an dass die nicht verschwinden, dass macht sonst Probleme. Wenn man sich die Definition von a_k anschaut, sieht man dass Paare von a_k's die zu komplex konjugierten lambda_k's gehören auch wieder komplex konjugiert sind. Wir können das folgich

so schreiben.

Dieser Term hier ist gerade unsere gesuchte gedämpfte Oszillation. Der Cosinus ist eine Oszillation und die Exponentialfunktion ist eine Dämpfung. Das ist genau das was wir zeigen wollten. Wenn wir ein Fokker-Planck System haben und zwei Observablen f und g zu den Zeitpunkten t und 0 betrachten, dann ist der Ensemble Mittelwert eine gedämpfte Oszillation, zumindest wenn wir es nur für große Zeiten betrachten und die Eigenwerte so liegen wie im rechten Diagram.

Der Imaginärteil von lambda_1 ist unser omega und der negative Realteil is unser gamma. Dann ist das konsistent zu den anderen Gleichungen in diesem Vortrag. Das Verhältnis von omega und gamma ist wieder unsere gesuchte Robustness. Wir wissen also jetzt, dass die beiden Eigenwerte mit dem größten Realteil der nicht 0 ist, entscheidend sind. Die nennt man auch die ersten nichttrivialen Eigenwerte. Wenn wir dieses Paar kennen, können wir die Robustness berechnen.

15:00

Das ist auch die perfekte Überleitung zum nächsten Abschnitt, denn jetzt werden wir versuchen diesen Eigenwert zu berechnen.

Wir starten mit der Eigenwertgleichung des Fokker-Planck Operators. Im Allgemeinen kann ich keine diese Gleichung nicht Lösen, also beschränken uns auf den

eindimensionalen Fall und erhalten diese Differentialgleichung für v. Da wir Oszillationen sehen wollen, nehmen wir außerdem

periodische Randbedingungen an.

Mit diesem Ansatz hier

wird aus der Eigenwertgleichung eine Differentialgleichung für g. Ich schreibe die hier nicht hin, denn wir kennen sie zwar, sie passt aber nicht auf das Bild und sie ist auch nicht wichtig.

Außerdem werden die periodischen Randbedingungen zu diesem Integral von g. Was f ist steht aus Platzgründen nicht da, ist auch nicht wichtig. Die periodischen Randbedingungen sind erfüllt, wenn diese Gleichung für eine beliebige ganze Zahl k erfüllt ist. Es ist kein Zufall, das wir diese ganze Zahl k nennen und im letzten Abschnitt über lambda_k geredet haben. Wir sehen später, dass wenn man k auf 0 oder +-1 setzt es auf lambda_0 oder lambda_+-1 führt.

Wir beschränken uns auf den weak-noise limit, betrachten also nur kleine epsilons

und finden so zwei mögliche g die die Differentialgleichung lösen.

Wenn wir g_1 in unsere Periodizitätsbedingung einsetzen und wieder für kleine epsilon nähern,

erhalten wir diesen Ausdruck für lambda. c_1 und c_2 sind dabei positive reelle konstanten.

Wenn wir g_2 in unsere Periodizitätsbedingung einsetzen und wieder für kleine epsilon nähern erhalten wir zwei weiter Ausdrücke für lambda. c_3, c_4, c_5 und c_6 sind ebenfalls positive reelle Konstanten. Jetzt ist die Sache die. Wir hatten im letzten Abschnitt gesagt, dass wir den Eigenwert mit dem größten Realteil der nicht 0 ist brauchen. Wir betrachten kleine epsilons, also ist der rechte Eigenwert der richtige.

Der rechte Eigenwert ausgeschreiben ist das hier. Setzen wir k=0 ein, erhalten wir lambda = 0 den Eigenwert des stationären Zustands. Setzen k=+-1 ein erhalten wir lambda_+-1 unser erstes nichtriviales komplex konjugiertes Paar Eigenwerte. Wie ich schon mal gesagt habe, ist die Dämpfungskonstante Gamma der negative Realteil und die Kreisfrequenz omega der Imaginärteil. Die Robustness R können wir berechnen in dem wir omega durch gamma teilen.

Damit kommen wir zu dem zentralen Ergebnis meiner Masterarbeit. Wenn euch jemand fragt, was ich in meiner Masterarbeit gemacht habe, das hier ist die Antwort: Die Robustness eines eindimensionalen Langevin-Systems mit periodischen Randbedingungen welches durch die obrige Fokker-Planck Gleichung beschrieben wird ist für kleine epsilon ... das hier. Das ist ein wünderschöner einfach zu berechnender Ausdruck.

19:10

Kommen wir zum näcshten Abschnitt. Jetzt wollen wir unser Ergebnis mit einer oberen Schranke vergleichen, die von Barato und Seifert, ihr kennt die beiden vielleicht, gefunden wurde.

Die beiden haben sich nicht kontinuierliche Langevin-Systeme wie ich sondern diskrete Markov Netzwerke angeschaut. Insbesondere so einen unicycle Netwerk wie hier dagestellt. Das System besteht aus N diskreten Zuständen, ich hab mal 5 gezeichnet, die wir E_i nennen. Mit den Raten k, kann es zum nächsten oder vorherigen Zustand im Zyklus übergehen.

Das System wird durch die Mastergleichung beschrieben. p ist ein N-dimensionaler vektor und die i-te komponente davon ist die Wahrscheinlichkeit im Zustand E_i zu sein. L ist eine NxN Matrix, die Übergangsmatrix

mit diesen Komponenten hier. Das ist eine tridiagonale Matrix, aber die obere rechte und untere linke Ecke der Matrix ist auch noch besetzt, da die Indizes periodisch sind. Vielleicht erinnert euch die Mastergleichung an etwas,

die sieht so ähnlich aus wie die Fokker-Planck Gleichung. Das ist auch kein Zufall, die Fokker-Planck Gleichung ist eigentlich nur ein Spezialfall von einer kontinuierlichen Mastergleichung. p ist hier kein Vektor mit endlicher Anzahl an Komponenten mehr, sondern eine kontinuierliche Funktion von q

und der eindimensionale Fokker-Planck Operator ist keine NxN Matrix, sondern ein kontinuierlicher linearer Operator. Wenn man den kontinuierlichen Fokker-Planck Operator diskretisiert und mit der Übergangsmatrix gleichsetzt erhält man

das hier. Delta q ist die Auflösung mit der man diskretisiert, also die Räumlichperiode P durch N. Im Limes N gegen unendlich verhalten sich beide Systeme gleich. Damit kann man Erkenntnnisse über diskrete unicylce System auf kontinuierliche eindimensionale Langevin Systeme anwenden.

Die Affinität des Zyklusses ist so definiert. Das ist gerade der Logarithmus aus der Wahrscheinlichkeit den Zyklus im Uhrzeigersinn zu durchgehen geteilt durch die Wahrscheinlichkeit den Zyklus entgegen den Uhrzeigersinn zu durchgehen. Wenn man jetzt obrige Gleichung einsetzt und den Kontinuumslimes macht

erhält man diesen Ausdruck für die Affinität. Das interessiert uns, denn Barato und Seifert vermuten, dass

das hier eine ober Schranke für die Robustness ist. Durch Einsetzen und wieder limes N gegen unendlich fahren erhalten wir

dieses Ergebnis. Also eine obere Schranke für die Robustness von eindimensionalen Langevin-Systemen.

Die Robustness die wir im letzten Abschnitt gefunden haben, sollte also stets unterhalb dieser Schranke liegen. Und das ist auch der Fall, denn das 1/2pi epsilon kürzt sich raus und das die linken Integrale kleiner als das rechte Integral ist, kann man mit dem Cauchy-Schwarz Theorem beweisen. Das gilt für beliebige Funktionen F und Q, solange die beiden positiv sind, was sie sind. (evtl. zusätzlich). Man beachte, dass unser Ergebnis für die Robustness nur für kleine epsilon gilt, aber die obere Schranke für alle epsilon gilt.

23:30

Nachdem wir uns die ganze Zeit mit statistischer Mechanik befasst haben, machen wir jetzt einen kleinen Exkurs in die deterministische Mechanik. Ich will nähmlich das Konzept von Grenzzyklen erklären.

Als einfachstes Beispiel nehmen wir den Stuart-Landau Oszillator. Das ist ein deterministisches System mit diesem Phasenporträt hier. Was fällt euch auf wenn ihr dieses Bild anschaut?

Alle Anfangspunkte führen auf diesen Zyklus. Der heißt Grenzzyklus und ist für den Stuart-Landau Oszillator ein Kreis. Wir interessieren uns auch für andere Systeme, die nur qualitativ so aussehen wie dieses Phasenporträt. Dann gibt es auch einen Grenzzyklus, der ist aber im Allgemeinen kein Kreis.

Wenn das System innerhalb des Grenzzyklusses anfängt, bewegt es sich auf einer Spirale nach außen und nähert sich ihm an.

Wenn das System hingegen außerhalb des Grenzzyklusses anfängt, bewegt es sich auf einer Spirale nach innen und nähert sich auch wieder dem Grenzzyklus an.

Wenn wir von deterministischer Dynamik zu Langevin-Dynamik übergehen, bleibt dieser Grenzzyklus bestehen. Das System ist zwar nicht exakt auf dem Grenzzyklus, bleibt aber stehts in seiner Umgebung, da es eine rückstellende Kraft gibt, die der Diffusion in Radialrichtung entgegenwirkt. Diese Animation erinnert euch vielleicht an etwas... Wir haben so etwas schon mal gesehen...

Das System sieht dem System vom Anfang meiner Präsentation verdächtig ähnlich. Das rechte System ist eindimensional und das linke ist zweidimensional. Im weak-noise limit ist das System auf

dieses unendlich enges Band beschränkt und somit effektiv eindimensional. Und für eindimensionale Systeme können wir die Robustness leicht berechnen. Deshalb liegt die Hoffnung nahe, mit einer eindimensionalen Beschreibung das zweidimensinale System im weak noise limit beschreiben zu können, und so dessen Robustness zu berechnen.

Es gibt da aber ein kleines Problem. Man sollte denken, dass man einfach die Bewegungsgleichung des linken Systems nehmen kann.

Und dann nur die Tangentialkomponente davon nimmt. Das kann man machen, aber dann kommt da eine zu kleine Diffusion raus. Der Grund hierfür nennt man Phasenkopplung.

Um das zu erklären,

schneiden wir Ring auf

und machen ihn gerade. Es gibt natürlich periodische Randbedingungen.

Angenommen, das Teilchen ist zum Beispiel hier

und bewegt sich dann aufgrund von Noise-Terms ein wenig in Radialrichtung.

Dann sollte man meinen, dass das egal ist, denn es gibt ja eine Rückstellkraft.

Wenn aber die Tangentialkomponente der Kraft abhängig von der radialen Position ist,

dann relaxiert der Radialteil zwar, aber die beiden Systeme haben einen unterschiedlichen Tangentialteil. Deshalb führt eine Diffusion in Radialrichtung indirekt zu einer Diffusion in Tangentialrichtung und kann folglich nicht einfach vernachlässigt werden. Das nennt man Phasenkopplung. Das ist die selbe epsilon Ordnung wie die direkte Tangentialdiffusion und kann deshalb auch im weak-noise limit nicht vernachlässigt. Die Stärke der Phasenkopplung ist zwar für den Stuart-Landau Oscillator analytisch berechenbar, aber für z.B. den Brüsselator hat das noch niemand hingekriegt. Wir können also diese effektive eindimensionale Beschreibung nicht verwenden um die Robustness zu berechnen, aber wir können sie verwenden um eine obere Schranke der Robustness zu berechnen. Denn die Phasenkopplung sorgt für zusätzliche Diffusion, und zusätzliche Diffusion führt zu einer kleineren Robustness.

28:20

Ich habe auch Numerik gemacht.

Sechs Monate Numerik lassen sich folgendermaßen zusammenfassen. Man kann das auch alles Numerisch machen, dann kommt das selbe raus wie wenn man es analytisch rechnet, oder zumindest nichts was dem widerspricht. Man kann ein Langevin-System eine Million mal simulieren, dann hat man eine Million q(t), dann rechnet man f(q(t))g(q(0)) aus und mittelt über diese Systeme. Dann sucht man die peaks. omega ergibt sich dann aus den Abständem und gamma erhält man indem man eine Exponentialfunktion fittet. Wichtig ist, dass man hinten fittet, da nur für große t der h(t) vernachlässigbar ist.

29:20

Zum Abschluss ein kleiner recap.

Wir haben gesehen, dass stochastische oszillierende Systeme eine stochastische, also nicht-konstante Periodendauer haben.

Und der Ensemblemittelwert macht gedämpfte Oszillationen.

Das haben wir auch mehr oder weniger Bewiesen. Der erste nichttriviale Eigenwert lambda+-1 gibt mit Realteil und Imaginärteil Dämpfung und Kreisfrequenz an.

Dann haben mit dieser Rechnung für eindimensionale Systeme im weak noise Limit

diese Robustness erhalten.

Dann haben wir den Kontinuumslimes von Barato und Seiferts Schranke gefahren und festgestellt, dass eine Schranke gefunden, die in der Tat über unserem Ergebnis für die Robustness liegt.

Dann haben wir gesehen, dass ein mehrdimensionales System mit Grenzzyklus effektiv gesehen eindimensional ist, es gibt aber eine schwierig zu berechnende Phasenkopplung.

Wir haben noch ... minuten, also will ich noch kurz sagen für was man das ganze braucht.
Ein Beispiel für oszillierende stochastische Systeme sind chemische Netzwerke. Die gibt es auch in der Natur, denn aus einem DNA-Abschnitt wird ein identischer mRNA-Strang transkribiert und anhand dieses mRNA-Strangs werden Protein synthetisiert. Wenn dieses Protein die Transkription des dazugehörigen DNA-Abschnitts hemmt, gibt es eine zeitlich verzögerte negative Rückkopplung und folglich Oszillationen. Lebewesen verwenden solche oszillierenden chemischen Netzwerke eingesetzt um den zeitlichen Ablauf anderer Prozesse zu regeln. Ein Beispiel hierfür ist der circadianer Rhythmus also der Tag-Nacht Rhythmus.
Die Robustness ist ein Maß dafür wie konstant die Periodendauer ist und folgich wie zeitlich präzise damit Prozesse geregelt werden können.

Das wars.
