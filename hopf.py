from common import *


class Hopf(MyScene):
    def construct(self):
        if LONG_WAITS:
            self.waittime = 24
        else:
            self.waittime = DEFAULT_WAIT_TIME

        # copied from def steward in small.py
        def steward(q, a, b, c, d):
            x, y, _z = q
            x -= offset
            r = np.sqrt(x**2 + y**2)
            theta = np.arctan2(y, x)
            drdt = a * r - b * r**3
            dthetadt = c + d * r**2
            dxdr = np.cos(theta)
            dxdtheta = -np.sin(theta) * r
            dydr = np.sin(theta)
            dydtheta = np.cos(theta) * r
            dxdt = drdt * dxdr + dthetadt * dxdtheta
            dydt = drdt * dydr + dthetadt * dydtheta
            return np.array([dxdt, dydt, 0])

        def wrap(t, q):
            return steward(q, sa, sb, sc, sd)

        def euler(mob, dt):
            q = mob.get_center()
            # sol = scipy.integrate.solve_ivp(wrap, (0, dt), q)
            # mob.move_to(sol.y[:, -1])
            steps = round(dt / 0.01)

            for i in range(steps):
                q += dt / steps * steward(q, sa, sb, sc, sd) + np.sqrt(
                    dt / steps
                ) * diff * np.array([np.random.normal(), np.random.normal(), 0])
            mob.move_to(q)

        self.subtitle(
            "Als einfachstes Beispiel nehmen wir den Stuart-Landau Oszillator. Das ist ein deterministisches System mit diesem Phasenporträt hier."
        )
        sa = 2
        sb = 1 / 2
        sc = 1 / 2
        sd = 1 / 2
        offset = -3
        diff = 0
        func = lambda q: steward(q, sa, sb, sc, sd)
        y_range = [-3, 3.5] if SUBTITLES_ON else None
        # See also: https://docs.manim.community/en/stable/reference/manim.mobject.vector_field.StreamLines.html#continuousmotion
        ar1 = ArrowVectorField(func, length_func=lambda x: 0, y_range=y_range)
        self.add(ar1)
        ar2 = ArrowVectorField(func, y_range=y_range, max_color_scheme_value=17)
        self.play(ar1.animate.become(ar2))
        self.next_section()
        self.subtitle(
            "Ihr sieht vielleicht, dass alle Anfangspunkte auf diesen Zyklus hinführen. Der heißt Grenzzyklus und ist für den Stuart-Landau Oszillator ein Kreis. Wir interessieren uns auch für andere Systeme, die nur qualitativ so aussehen wie dieses Phasenporträt. Dann gibt es auch einen Grenzzyklus, der ist aber im Allgemeinen kein Kreis."
        )
        r0 = np.sqrt(sa / sb)
        leftcircle = Circle(radius=r0, color=WHITE).move_to([offset, 0, 0])
        self.play(Create(leftcircle))
        self.next_section()

        self.subtitle(
            "Wenn das System innerhalb des Grenzzyklusses anfängt, bewegt es sich auf einer Spirale nach außen und nähert sich ihm an."
        )
        dot = Dot(radius=0.08, color=YELLOW)
        dot.move_to(np.array([1 + offset, 0, 0]))
        self.play(Create(dot))
        dot.add_updater(euler)
        self.wait(self.waittime)
        self.next_section()

        self.subtitle(
            "Wenn das System hingegen außerhalb des Grenzzyklusses anfängt, bewegt es sich auf einer Spirale nach innen und nähert sich auch wieder dem Grenzzyklus an."
        )
        dot.remove_updater(euler)
        self.wait()

        # dot.move_to(np.array([3, 0, 0]))
        self.play(dot.animate.move_to(np.array([5 + offset, 0, 0])))
        self.wait()
        dot.add_updater(euler)

        self.wait(self.waittime)

        self.subtitle(
            "Wenn wir von deterministischer Dynamik zu Langevin-Dynamik übergehen, bleibt dieser Grenzzyklus bestehen. Das System ist zwar nicht exakt auf dem Grenzzyklus, bleibt aber stehts in seiner Umgebung."
        )
        diff = 0.4
        self.next_section()
        self.wait(self.waittime)

        frame = Rectangle(width=7, height=8, color=BLACK, fill_opacity=1.0).move_to(
            [4, 0, 0]
        )
        if SUBTITLES_ON:
            frame.shift([0, 1, 0])
        # self.play(GrowFromEdge(frame, LEFT))

        right_offset = 4

        rightcircle = Circle(radius=r0, color=WHITE).move_to([right_offset, 0, 0])

        vline = Line(
            np.array([right_offset, -AXS_RAD, 0]), np.array([right_offset, +AXS_RAD, 0])
        )
        hline = Line(
            np.array([right_offset - AXS_RAD, 0, 0]),
            np.array([right_offset + AXS_RAD, 0, 0]),
        )

        def synchronous_dot():
            diff = dot.get_center() - np.array([offset, 0, 0])
            phi = np.arctan2(diff[1], diff[0])
            return Dot(radius=0.08, color=YELLOW).move_to(
                [right_offset + r0 * np.cos(phi), r0 * np.sin(phi), 0]
            )

        right_dot = always_redraw(synchronous_dot)

        self.subtitle(
            "Dieses Bild erinnert euch vielleicht an etwas, wir haben so etwas schonmal gesehen, ganz am Anfang der Präsentation."
        )
        # self.play(FadeIn(frame))
        # self.play(Create(rightcircle))
        # self.play(Create(vline), Create(hline))
        # self.add(always_redraw(synchronous_dot))
        self.next_section()
        fadetime = 2
        self.play(FadeIn(frame, run_time=fadetime))
        self.play(
            FadeIn(rightcircle, run_time=fadetime),
            FadeIn(vline, run_time=fadetime),
            FadeIn(hline, run_time=fadetime),
        )
        self.add(right_dot)

        outer = Circle(radius=r0 + 0.2).move_to([offset, 0, 0])
        inner = Circle(radius=r0 - 0.2).move_to([offset, 0, 0])
        ring = Difference(outer, inner, color=BLUE, fill_opacity=0.5)
        self.subtitle(
            "Im weak-noise limit ist das System auf ein unendlich enges Band beschränkt und folglich effektiv eindimensional. Wir können also unser Wissen über eindimensionale Langevin-Systeme auch auf mehrdimensionale Systeme anwenden."
        )
        self.wait(self.waittime)
        self.next_section()
        self.subtitle(
            "Es gibt da aber ein kleines Problem. Man sollte denken, dass man einfach die Bewegungsgleichung von diesem System nehmen kann."
        )
        banner = Rectangle(width=15, height=1, color=BLACK, fill_opacity=1.0).move_to(
            [0, 3.5, 0]
        )
        self.play(FadeIn(ring))
        self.wait(self.waittime)
        self.next_section()
        self.play(FadeIn(banner, run_time=fadetime))
        langeND = MathTex(
            r"\partial_t \mathbf q(t) = ",
            r"\left( \left(",
            r"\mathbf F(\mathbf q(t)) + \sqrt{\epsilon} \underline{\underline{C}}(\mathbf q(t)) \boldsymbol\zeta(t) \right) ",
            r"\cdot \mathbf e_\varphi \right) \cdot \mathbf e_\varphi",
        ).move_to([0, 3.5, 0])
        self.play(Write(langeND[0]), Write(langeND[2]))
        self.wait(self.waittime)

        self.next_section("before_summary")
        self.subtitle(
            "Und dann nur die Tangentialkomponente davon nimmt. Das kann man machen, aber dann kommt da eine zu kleine Diffusion raus. Der Grund hierfür nennt man Phasenkopplung."
        )
        self.play(Write(langeND[1]), Write(langeND[3]))
        self.wait(self.waittime)

        self.next_section("summary")
        self.wait(self.waittime)
        self.next_section("after_summary")
        self.play(
            Uncreate(ar1),
            Uncreate(langeND),
            Uncreate(rightcircle),
            Uncreate(vline),
            Uncreate(hline),
            Uncreate(right_dot),
        )
        self.remove(frame, banner)
        dot.remove_updater(euler)
        self.wait()
        self.next_section()

        self.subtitle(
            "Um das zu erklären, schneiden wir Ring auf und machen ihn gerade. Es gibt natürlich periodische Randbedingungen."
        )
        cut = Rectangle(width=0.2, height=1).move_to([offset, r0, 0])
        cutted = Difference(ring, cut, color=BLUE, fill_opacity=0.5)
        self.next_section()
        self.play(FadeIn(cutted), FadeOut(ring))
        trans = Trans()
        self.next_section()
        self.play(
            Transform(
                cutted, trans.rect
            ),  # todo https://docs.manim.community/en/stable/reference/manim.animation.movement.Homotopy.html
            Transform(leftcircle, trans.perline),
            dot.animate.move_to(np.array([0, 0, 0])),
        )
        # self.wait()

        self.remove(dot)
        dot = Dot(radius=0.08, color=YELLOW)
        trans = Trans()
        self.add(trans.rect, trans.perline, dot)

        sa = 2
        sb = 1 / 2
        sc = 1 / 2
        sd = 1 / 2
        r0 = np.sqrt(sa / sb)

        def linear_2D(mob, dt):
            q = mob.get_center()
            steps = round(dt / 0.01)

            for i in range(steps):
                r = r0 + q[1]

                # drdt = sa * r - sb * r ** 3
                # dthetadt = sc + sd * r ** 2
                # tayloring:
                drdt = sa * r0 - sb * r0**3 - 3 * sb * r0**2 * (r - r0)
                dthetadt = sc + sd * r0**2 + 2 * r0 * sd * (r - r0)

                q += dt / steps * np.array([dthetadt, drdt, 0]) + np.sqrt(
                    dt / steps
                ) * diff * np.array([np.random.normal(), np.random.normal(), 0])

            if q[0] > 6:
                q[0] -= 12
            if q[0] < -6:
                q[0] += 12
            mob.move_to(q)

        dot.add_updater(linear_2D)
        diff = 0.3
        self.wait(self.waittime)
        self.next_section()

        dot.remove_updater(linear_2D)
        self.subtitle("Angenommen, das Teilchen ist zum Beispiel hier")
        # self.wait()
        # self.play(dot.animate.move_to([0, 0, 0]))
        dot.move_to([0, 0, 0])
        self.wait()
        self.next_section()
        self.subtitle(
            "und bewegt sich dann aufgrund von Noise-Terms ein wenig in Radialrichtung."
        )
        lowerdot = dot.copy()
        self.add(lowerdot)
        self.play(dot.animate.move_to([0, 1, 0]), lowerdot.animate.move_to([0, -1, 0]))
        self.next_section()
        self.subtitle(
            "Dann sollte man meinen, dass das egal ist, denn es gibt ja eine Rückstellkraft."
        )
        self.play(
            Create(Arrow([0, 1, 0], [0, 0.2, 0], buff=0)),
            Create(Arrow([0, -1, 0], [0, -0.2, 0], buff=0)),
        )
        self.next_section()
        self.subtitle(
            "Wenn aber die Tangentialkomponente der Kraft abhängig von der radialen Position ist,"
        )
        self.play(
            Create(Arrow([0, 1, 0], [1.3, 1, 0], buff=0)),
            Create(Arrow([0, -1, 0], [1, -1, 0], buff=0)),
        )
        self.wait()
        self.next_section()
        diff = 0
        slowfact = 10
        sa /= slowfact
        sb /= slowfact
        sc /= slowfact
        sd /= slowfact
        dot.add_updater(linear_2D)
        lowerdot.add_updater(linear_2D)
        self.subtitle(
            "dann relaxiert der Radialteil zwar, aber die beiden Systeme haben einen unterschiedlichen Tangentialteil. Deshalb führt eine Diffusion in Radialrichtung indirekt zu einer Diffusion in Tangentialrichtung und kann folglich nicht einfach vernachlässigt werden. Das nennt man Phasenkopplung. Das ist die selbe epsilon Ordnung wie die direkte Tangentialdiffusion und kann deshalb nicht einfach vernachlässigt werden. Die Stärke der Phasenkopplung ist zwar für den Stuart-Landau Oscillator analytisch berechenbar, aber für z.B. den Brüsselator hat das noch niemand hingekriegt."
        )
        self.wait(self.waittime)
