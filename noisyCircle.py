from common import *


class NoisyCircle(MyScene):
    def construct(self):
        self.definitions()
        self.step1()

    def definitions(self):
        self.origin_point = np.array([-4, 0, 0])
        self.radius = 2
        self.circle = Circle(radius=self.radius)
        self.circle.move_to(self.origin_point)
        self.define_axis()

        self.maxline = Line(
            np.array([-8, self.origin_point[1] + self.radius, 0]),
            np.array([+8, self.origin_point[1] + self.radius, 0]),
            stroke_width=2,
        )
        self.minline = Line(
            np.array([-8, self.origin_point[1] - self.radius, 0]),
            np.array([+8, self.origin_point[1] - self.radius, 0]),
            stroke_width=2,
        )

        self.framedt = 1 / 15

        if LONG_WAITS:
            self.waittime = 24
        else:
            self.waittime = DEFAULT_WAIT_TIME

    def define_axis(self):
        x_start = np.array(self.origin_point + [-AXS_RAD, 0, 0])
        x_end = np.array([7, 0, 0])

        y_start = np.array(self.origin_point + [0, -AXS_RAD, 0])
        y_end = np.array(self.origin_point + [0, +AXS_RAD, 0])

        x_axis = Line(x_start, x_end)
        y_axis = Line(y_start, y_end)

        self.x_axis = x_axis
        self.y_axis = y_axis

    def pre_calc(self, rate, diff, T, phi0):
        num_systems = 20
        steps = int(round(T / FRAMEDT)) + 2
        phis = np.ones((steps, num_systems)) * phi0
        for i in range(1, steps):
            phis[i] = (
                phis[i - 1]
                + np.ones(num_systems) * FRAMEDT * rate
                + np.sqrt(FRAMEDT) * diff * np.random.normal(size=num_systems)
            )
        return phis

    def step1(self):
        # Mitigation for https://github.com/ManimCommunity/manim/issues/2572
        additional_points = None
        num_additional = None
        additional_phis = None

        self.t_offset = 0
        self.phi = 0
        curve_lhs = -1
        curve_rhs = 6

        rate = 0
        diff = 0

        class State(Enum):
            INACTIVE = 0
            PRE_MAX = 1
            PRE_MIN = -1

        self.state = State.INACTIVE

        maxs = []
        mins = []
        self.markers = VGroup()

        self.future = self.pre_calc(0, 0, 100, 0)

        self.count_to_next_min = None
        self.count_to_next_max = None

        def go_around_circle(mob, dt):
            self.t_offset += dt
            self.last_dt = dt

            if dt == 0:
                return
            if abs(1 - dt / FRAMEDT) > 0.1:
                print("weird dt:", dt)

            self.phi = self.future[0, 0]
            self.future = np.delete(self.future, obj=0, axis=0)
            if self.count_to_next_min is not None:
                self.count_to_next_min -= 1
            if self.count_to_next_max is not None:
                self.count_to_next_max -= 1

            # self.phi += dt * rate + np.sqrt(dt) * diff * np.random.normal()

            # if (
            #     self.state == State.PRE_MAX
            #     and self.phi % 1 >= 1 / 4
            #     and self.phi % 1 < 1 / 2
            # ):
            #     cr = Cross(scale_factor=0.08)
            #     cr.move_to(np.array([curve_lhs, self.origin_point[1] + self.radius, 0]))
            #     self.markers.add(cr)
            #     if len(maxs) != 0:
            #         deltat = self.t_offset - maxs[-1]
            #         b1 = Brace(Line(
            #             np.array([curve_lhs, self.origin_point[1] + self.radius, 0]),
            #             np.array([curve_lhs + deltat, self.origin_point[1] + self.radius, 0])
            #             ), direction=np.array([0, 1, 0]))
            #         b1text = b1.get_text("%.3f" % deltat)
            #         self.markers.add(b1, b1text)
            #     maxs.append(self.t_offset)
            #     self.state = State.PRE_MIN
            # if self.state == State.PRE_MIN and self.phi % 1 >= 3 / 4:
            #     cr = Cross(scale_factor=0.08)
            #     cr.move_to(np.array([curve_lhs, self.origin_point[1] - self.radius, 0]))
            #     self.markers.add(cr)
            #     if len(mins) != 0:
            #         deltat = self.t_offset - mins[-1]
            #         b1 = Brace(Line(
            #             np.array([curve_lhs, self.origin_point[1] - self.radius, 0]),
            #             np.array([curve_lhs + deltat, self.origin_point[1] - self.radius, 0])
            #             ), direction=np.array([0, -1, 0]))
            #         b1text = b1.get_text("%.3f" % deltat)
            #         self.markers.add(b1, b1text)
            #     mins.append(self.t_offset)
            #     self.state = State.PRE_MAX

            mob.move_to(self.circle.point_from_proportion(self.phi % 1))

        self.subtitle(
            "Zu Beginn will ich verständlich machen, wieso Langevin-Systeme gedämpfte Oszillationen zeigen können. Dazu betrachten wir das simpelste Beispiel-System. Angenommen wir haben ein Kreis"
        )
        self.play(Create(self.circle))

        self.next_section()
        self.subtitle("und einen Punkt auf diesem Kreis.")
        origin_point = self.origin_point

        dot = Dot(radius=0.08, color=YELLOW)
        dot.add_updater(go_around_circle)
        self.play(Create(dot))

        self.next_section()
        self.subtitle("der sich mit konstanter Geschwindigkeit bewegt.")
        eqn = MathTex(r"\dot q = F_0", r" + \sqrt{\epsilon} C_0 \zeta(t)").move_to(
            np.array([-4.5, 3, 0])
        )
        obs1 = MathTex(r"\sin q(t) = \sin(F_0 t)").move_to(np.array([-4.5, -3, 0]))
        obs2 = MathTex(r"\sin q(t)").move_to(np.array([-4.5, -3, 0]))
        obs3 = MathTex(r"\langle \sin q(t)\rangle").move_to(np.array([-4.5, -3, 0]))
        self.play(Write(eqn[0]))
        rate = 0.25
        self.future = self.pre_calc(0.25, 0, 100, 0)
        self.wait(self.waittime)

        self.next_section()
        self.subtitle("Wenn wir uns jetzt eine Koordinate anschauen")
        self.play(
            Create(self.x_axis),
            Create(self.y_axis),
            Create(self.minline),
            Create(self.maxline),
            Write(obs1),
        )

        t_zero = self.t_offset
        self.state = (
            State.PRE_MIN
            if self.phi % 1 > 1 / 4 and self.phi % 1 < 3 / 4
            else State.PRE_MAX
        )

        def get_line_to_curve():
            return Line(
                dot.get_center(),
                np.array([curve_lhs, dot.get_center()[1], 0]),
                color=YELLOW_A,
                stroke_width=2,
            )

        self.curve = VGroup()

        flag_average = False

        last_peak = None
        self.new_peak_method = False

        def get_curve():
            for line in self.curve:
                line.shift(self.last_dt * RIGHT)

            if not flag_average:
                col = YELLOW_D
                y = dot.get_center()[1]
                ourphi = self.phi
            else:
                col = ORANGE
                y = dot.get_center()[1]
                ourphi = self.phi
                for adot in additional_points:
                    y += adot.get_center()[1]
                for phi in additional_phis:
                    ourphi += phi
                y /= 1 + num_additional
                ourphi /= 1 + num_additional

            if not self.new_peak_method:
                max_now = (
                    self.state == State.PRE_MAX
                    and ourphi % 1 >= 1 / 4
                    and ourphi % 1 < 1 / 2
                )
            else:
                max_now = (
                    self.count_to_next_max is not None and self.count_to_next_max < -1
                )
                if max_now:
                    self.count_to_next_max = None
            if not self.new_peak_method:
                min_now = self.state == State.PRE_MIN and ourphi % 1 >= 3 / 4
            else:
                min_now = (
                    self.count_to_next_min is not None and self.count_to_next_min < -1
                )
                if min_now:
                    self.count_to_next_min = None

            periodtime = int(
                round(2 * np.pi / rate / FRAMEDT)
            )  # todo warum ist da ein 2pi
            avgfuture = np.mean(np.sin(2 * np.pi * self.future), axis=1)
            next_period = avgfuture[0:periodtime]

            if max_now and not flag_average:
                cr = Cross(scale_factor=0.08)
                cr.move_to(np.array([curve_lhs, y, 0]))
                self.markers.add(cr)
                if len(maxs) != 0:
                    deltat = self.t_offset - maxs[-1]
                    b1 = Brace(
                        Line(
                            np.array(
                                [curve_lhs, self.origin_point[1] + self.radius, 0]
                            ),
                            np.array(
                                [
                                    curve_lhs + deltat,
                                    self.origin_point[1] + self.radius,
                                    0,
                                ]
                            ),
                        ),
                        direction=np.array([0, 1, 0]),
                    )
                    b1text = b1.get_text("%.2f" % deltat)
                    self.markers.add(b1, b1text)
                maxs.append(self.t_offset)
                last_peak = self.t_offset
                self.state = State.PRE_MIN
                if flag_average:
                    self.count_to_next_min = np.argmin(next_period)
                    self.new_peak_method = True
            if min_now and not flag_average:
                cr = Cross(scale_factor=0.08)
                cr.move_to(np.array([curve_lhs, y, 0]))
                self.markers.add(cr)
                if len(mins) != 0:
                    deltat = self.t_offset - mins[-1]
                    b1 = Brace(
                        Line(
                            np.array(
                                [curve_lhs, self.origin_point[1] - self.radius, 0]
                            ),
                            np.array(
                                [
                                    curve_lhs + deltat,
                                    self.origin_point[1] - self.radius,
                                    0,
                                ]
                            ),
                        ),
                        direction=np.array([0, -1, 0]),
                    )
                    b1text = b1.get_text("%.2f" % deltat)
                    self.markers.add(b1, b1text)
                mins.append(self.t_offset)
                last_peak = self.t_offset
                self.state = State.PRE_MAX
                if flag_average:
                    self.count_to_next_max = np.argmax(next_period)
                    self.new_peak_method = True

            lp = np.array([curve_lhs, y, 0])
            if len(self.curve) == 0:
                new_line = Line(lp, lp, color=col)
            else:
                rp = self.curve[-1].get_end()
                new_line = Line(rp, lp, color=col)
            self.curve.add(new_line)

            return self.curve

        def get_periods():
            for el in self.markers:
                el.shift(self.last_dt * RIGHT)
            return self.markers

        dot_to_curve_line = always_redraw(get_line_to_curve)
        sine_curve_line = always_redraw(get_curve)
        periods = always_redraw(get_periods)
        self.subtitle(
            "dann sehen wir, das der Zeitverlauf sinusförmig ist. Insbesondere hat jeder Peak die selbe Höhe und den selben Abstand zum vorherigen Peak, in diesem Fall 4."
        )
        self.add(dot_to_curve_line, sine_curve_line, periods)
        self.wait(self.waittime)

        self.next_section("Single Noisy")
        self.subtitle(
            "Wenn wir allerdings zu der konstanten Rate F_0 noch Noise hinzufügen, sind alle Peaks zwar immernoch gleich hoch, aber die Abstände der Peaks sind nicht mehr gleichmäßig."
        )
        diff = 0.05
        self.future = self.pre_calc(rate, diff, 100, self.future[0][0])
        self.play(Write(eqn[1]), Transform(obs1, obs2))
        self.wait(self.waittime)

        self.next_section("Ensemble Average")

        def calc_additional_points():
            for i in range(num_additional):
                additional_phis[i] = self.future[0][i + 1]
                # additional_phis[i] += (
                #     self.last_dt * rate
                #     + np.sqrt(self.last_dt) * diff * np.random.normal()
                # )
                additional_points[i].move_to(
                    self.circle.point_from_proportion(additional_phis[i] % 1)
                )
            return additional_points

        num_additional = 19
        additional_phis = np.repeat(self.phi, num_additional)
        additional_points = VGroup()
        for phi in additional_phis:
            dot = Dot(radius=0.08, color=YELLOW)
            dot.move_to(self.circle.point_from_proportion(self.phi % 1))
            additional_points.add(dot)
        self.future = self.pre_calc(rate, diff, 100, self.future[0][0])
        add_points = always_redraw(calc_additional_points)
        self.subtitle(
            "Wenn wir ein Ensemble aus 20 identischen und unabhängigen System simulieren und den Ensemblemittelwert betrachten, werden die Abstände der Peaks gleichmäßiger, aber die Peaks fallen mit der Zeit ab."
        )
        self.add(add_points)
        # self.wait(1 / rate / 2)
        self.remove(dot_to_curve_line)
        flag_average = True

        self.play(Transform(obs2, obs3))
        self.wait(self.waittime)
        self.wait(10)
