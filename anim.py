# manim --verbosity WARNING --save_sections --write_all anim.py

import noisyCircle
import small
import cosexp
import eigvals
import lambdas
import integral
import myres
import hopf
import border
import numerics

# import phaseCoupling
# class PhaseCoupling(phaseCoupling.PhaseCoupling):
#     pass


class Numerics(numerics.Numerics):
    pass


class Border(border.Border):
    pass


class Hopf(hopf.Hopf):
    pass


class MyRes(myres.MyRes):
    pass


class Integral(integral.Integral):
    pass


class Lambdas(lambdas.Lambdas):
    pass


class Eigvals(eigvals.Eigvals):
    pass


class Cosexp(cosexp.Cosexp):
    pass


class Small(small.Small):
    pass


class NoisyCircle(noisyCircle.NoisyCircle):
    pass
