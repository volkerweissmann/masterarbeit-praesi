from common import *


class Border(MyScene):
    def construct(self):
        N = 5
        center = np.array([0, 0, 0])
        radius = 3
        states = []
        for i in range(N):
            phi = i * 2 * np.pi / N
            states.append(
                MathTex("E_" + str(i))
                .move_to(center + radius * np.array([np.sin(phi), np.cos(phi), 0]))
                .set_color(YELLOW)
            )
        for i in range(N):
            if i == 0:
                lower = N - 1
                higher = 0
            else:
                lower = i - 1
                higher = i
            ar = DoubleArrow(states[lower], states[higher])
            self.add(ar)
            phi2 = (i - 1 / 2 + 0.2) * 2 * np.pi / N
            self.add(
                MathTex("k^+_" + str(lower)).move_to(
                    center + (radius + 0.2) * np.array([np.sin(phi2), np.cos(phi2), 0])
                )
            )
            phi1 = (i - 1 / 2 - 0.3) * 2 * np.pi / N
            self.add(
                MathTex("k^-_" + str(higher)).move_to(
                    center + (radius - 1) * np.array([np.sin(phi1), np.cos(phi1), 0])
                )
            )

        self.add(*states)
        self.wait()
        self.next_section()
        self.clear()

        eqs = 0.7
        delta = -1.2
        offset = 3.3

        left = -3.7
        right = +3.5

        line2 = offset + delta * 0
        line3 = offset + delta * 1
        line4 = offset + delta * 2
        line5 = offset + delta * 3
        line6 = offset + delta * 4
        line7 = offset + delta * 5

        mastereq = (
            MathTex(r"\partial_t \mathbf p(t) = \underline{\underline L}\mathbf p(t)")
            .scale(eqs)
            .move_to([left, line2, 0])
        )
        trm = (
            MathTex(
                r"L_{ij} = k^+_j\delta_{i-1,j} + k^-_j\delta_{i+1,j} - \left(k^-_j+k^+_j\right)\delta_{i,j}"
            )
            .scale(eqs)
            .move_to([left, line3, 0])
        )
        fpeq = (
            MathTex(r"\partial_t p(q,t) = \hat{\mathcal L}p(q,t)")
            .scale(eqs)
            .move_to([right, line2, 0])
        )
        fop = (
            MathTex(
                r"\hat{\mathcal L}p = -\sum_i \partial q_i\left(F_i p\right) + \epsilon \sum_i\sum_j \partial q_i\partial q_j\left(Q_{ij}p\right)"
            )
            .scale(eqs)
            .move_to([right, line3, 0])
        )
        affinity = (
            MathTex(r"\mathcal A=\left|\ln\prod_{i=1}^N \frac{k^+_i}{k^-_i}\right|")
            .scale(eqs)
            .move_to([left, line5, 0])
        )
        contaffinity = (
            MathTex(r"\mathcal A=\int_0^P \frac{F(q)}{\epsilon Q(q)}\mathrm dq")
            .scale(eqs)
            .move_to([right, line5, 0])
        )
        discretebound = (
            MathTex(r"R\leq \cot\frac{\pi}{N}\tanh\frac{\mathcal A}{2N}")
            .scale(eqs)
            .move_to([left, line6, 0])
        )
        contbound = (
            MathTex(r"R\leq\frac{1}{2\pi\epsilon}\int_0^P\frac{F(q)}{Q(q)}\mathrm dq")
            .scale(eqs)
            .move_to([right, line6, 0])
        )
        comparison = (
            MathTex(
                r"R = \frac{1}{2\pi\epsilon}\left(\int_0^P\frac{1}{F(q)}\mathrm dq\right)^2\left(\int_0^P\frac{Q(q)}{F^3(q)}\mathrm dq\right)^{-1}\leq\frac{1}{2\pi\epsilon}\int_0^P\frac{F(q)}{Q(q)}\mathrm dq"
            )
            .scale(eqs)
            .move_to([0, line7, 0])
        )
        anal = (
            MathTex(
                r"k_i^\pm = \frac{\epsilon Q((i\pm 1)\Delta q)}{\Delta q^2}\exp\left(\pm(\frac{\left( F((i\pm 1)\Delta q) - 2\epsilon Q'((i\pm 1)\Delta q)\right)\Delta q}{2\epsilon Q((i\pm 1)\Delta q)}\right)"
            )
            .scale(eqs)
            .move_to([0, line4, 0])
        )

        self.play(Write(mastereq))
        self.next_section()
        self.play(Write(trm))
        self.next_section()
        self.play(Write(fpeq))
        self.next_section()
        self.play(Write(fop))
        self.next_section()
        self.play(Write(anal))
        self.next_section()
        self.play(Write(affinity))
        self.next_section()
        self.play(Write(contaffinity))
        self.next_section()
        self.play(Write(discretebound))
        self.next_section()
        self.play(Write(contbound))
        self.next_section("secondlast")
        self.play(Write(comparison))
        self.next_section("last")
        self.wait()
