from common import *


class Lambdas(MyScene):
    def construct(self):
        left_edge = -6.5
        pos2 = np.array([left_edge, 3.5, 0])
        lfgr = MathTex(
            r"&{{\langle f(\mathbf q(t)) g(\mathbf q(0)) \rangle}}\\&{{=}} {{\sum_k a_k}} {{e^{\lambda_k t}}}",
        ).move_to(pos2, aligned_edge=UL)
        lfgr2 = MathTex(
            r"&{{\langle f(\mathbf q(t)) g(\mathbf q(0)) \rangle}}\\&{{=}} {{\sum_k a_k}} {{e^{\Re\lambda_k t} e^{i\Im\lambda_k t} }} ",
        ).move_to(pos2, aligned_edge=UL)
        lfgr3 = MathTex(
            r"&{{\langle f(\mathbf q(t)) g(\mathbf q(0)) \rangle}}\\&{{=}} {{a_0}}\\& + a_{+1} e^{\Re\lambda_{+1} t} e^{i\Im\lambda_{+1} t} \\&+ a_{-1} e^{\Re\lambda_{-1} t} e^{i\Im\lambda_{-1} t} \\&{{+h(t)}}",
        ).move_to(pos2, aligned_edge=UL)
        lfgr4 = MathTex(
            r"&{{\langle f(\mathbf q(t)) g(\mathbf q(0)) \rangle}}\\&{{=}}  {{a_0}} \\& + {{2|a_{1}|\cos(\Im\lambda_1 t + \varphi(a_1)) e^{\Re\lambda_{1} t}}} \\&{{+h(t)}}",
        ).move_to(pos2, aligned_edge=UL)

        self.add(lfgr)
        self.subtitle(
            "Jetzt haben wir diesen Ausdruck und manch einer sieht schon woher die gedämpften Oszillationen kommen. Wir schauen uns hierfür erst einmal die Struktur der Eigenwerte lambda_k an."
        )
        self.wait()
        self.next_section()

        ax = Axes(
            x_range=[-1, 1, 1],
            y_range=[-2, 1, 1],
            x_length=3,
            axis_config={"include_ticks": False},
            # axis_config={"color": GREEN},
            # x_axis_config={"rotation": np.pi},
            # tips=False,
        ).move_to([3.5, 0, 0])
        labels = ax.get_axis_labels(
            x_label=Tex(r"$\Im\lambda$"), y_label=Tex(r"$\Re\lambda$")
        )
        self.subtitle(
            "Wenn man die Eigenwerte in der komplexen Ebene einzeichnet, so sieht typischerweise folgendermaßen aus. Ich sage typischerweise, denn in meiner Masterarbeit habe ich angenommen, dass das so ist, aber nur teilweise bewiesen das es so ist. Wenn es anders ist haben wir ein Problem."
        )
        self.play(Create(ax), Write(labels))
        self.next_section()
        cross1 = Cross(scale_factor=0.08).move_to(ax.coords_to_point(0, 0))
        cross2 = Cross(scale_factor=0.08).move_to(ax.coords_to_point(1, -1))
        cross3 = Cross(scale_factor=0.08).move_to(ax.coords_to_point(-1, -1))
        self.subtitle(
            "Es gibt einen Eigenwert, der 0 ist. Die dazugehörige Eigenfunktion ist der stationäre Zustand und wir nennen ihn lambda_0."
        )
        self.play(Create(cross1))
        self.play(Write(MathTex(r"\lambda_0").move_to(ax.coords_to_point(-0.3, 0.2))))
        self.next_section()
        self.subtitle(
            "Es gibt zwei komplex konjugierte Eigenwerte mit einem negativen Realteil, wir nennen diese lambda_+1 und lambda_-1"
        )
        self.play(Create(cross2), Create(cross3))
        self.play(
            Write(MathTex(r"\lambda_{+1}").move_to(ax.coords_to_point(1, -0.8))),
            Write(MathTex(r"\lambda_{-1}").move_to(ax.coords_to_point(-1, -0.8))),
        )

        # n = 256
        # imageArray = np.uint8(
        #     [[[0, 256 - i * 256 / n, 0] for _ in range(0, n)] for i in range(0, n)]
        # )
        # image = (
        #     ImageMobject(imageArray)
        #     .scale(2)
        #     .move_to(ax.coords_to_point(0, -1), aligned_edge=UP)
        # )
        # self.add(image)

        width = 1.2
        para = -0.6
        parb = -1.7
        pard = -1.1
        pare = 1
        self.subtitle(
            "Alle anderen Eigenwerte liegen unterhalb der grünen Linie, haben also einen kleineren Realteil."
        )
        self.play(
            Create(
                Line(
                    ax.coords_to_point(-1.5, -1),
                    ax.coords_to_point(+1.5, -1),
                    color=GREEN,
                )
            ),
            Create(
                Line(
                    ax.coords_to_point(para - pare, parb),
                    ax.coords_to_point(para + width - pare, pard),
                    color=GREEN,
                )
            ),
            Create(
                Line(
                    ax.coords_to_point(para, parb),
                    ax.coords_to_point(para + width, pard),
                    color=GREEN,
                )
            ),
            Create(
                Line(
                    ax.coords_to_point(para + pare, parb),
                    ax.coords_to_point(para + width + pare, pard),
                    color=GREEN,
                )
            ),
        )
        self.subtitle(
            "Wenn man das so schreibt... und annimmt das kein a_k verschwindet sieht man, dass nur die Summanden mit den größten Realteilen wichtig sind, da wir uns nur für das Verhalten für große Zeiten interessieren."
        )
        self.next_section()
        self.play(TransformMatchingTex(lfgr, lfgr2))
        self.subtitle(
            "Mit dem Diagram rechts wird das also zu dem hier. Die Summanden die wir vernachlässigen, da sie schneller abfallen habe ich als h(t) zusammengefasst."
        )
        self.next_section()
        self.play(TransformMatchingTex(lfgr2, lfgr3))
        self.subtitle(
            "Wenn man sich die Definition von a_k anschauen würde, sieht man dass a_k's die zu komplex konjugierten Eigenwerten gehören auch komplex konjugiert sind. Damit kann man das ganze so schreiben. "
        )
        self.next_section()
        self.play(TransformMatchingTex(lfgr3, lfgr4))
        self.subtitle(
            "Dieser Term hier ist eine gedämpfte Oszillation. Das ist genau das was wir zeigen wollten. Wenn wir ein Fokker-Planck System haben und zwei Observablen f und g zu den Zeitpunkten t und 0 betrachten, dann ist der Ensemble Mittelwert eine gedämpfte Oszillation, zumindest wenn wir es nur für große Zeiten betrachten und die Eigenwerte so liegen wie im rechten Diagram."
        )
        self.next_section()
        self.secind([lfgr4[7]], "secondlast")
        eqo = MathTex(
            r"\omega=\Im\lambda_1 > 0",
        ).move_to([left_edge, 0, 0], aligned_edge=UL)
        eqg = MathTex(
            r"\gamma= -\Re\lambda_1 > 0",
        ).move_to([left_edge, -1, 0], aligned_edge=UL)
        eqr = MathTex(
            r"R= \frac{\omega}{\gamma}",
        ).move_to([left_edge, -2, 0], aligned_edge=UL)
        self.subtitle(
            "Der Imaginärteil von lambda_1 ist unser omega und der negative Realteil is unser gamma. Dann ist das konsistent zu den anderen Gleichungen in diesem Vortrag."
        )
        self.play(Write(eqo))
        self.play(Write(eqg))
        self.subtitle(
            "Das Verhältnis von omega und gamma ist wieder unsere gesuchte Robustness. Wir wissen also jetzt, dass die beiden Eigenwerte mit dem größten Realteil der nicht 0 ist, entscheidend sind. Wenn wir dieses Paar kennen, können wir die Robustness berechnen. Das ist auch die perfekte Überleitung zum nächsten Abschnitt, denn da werden wir versuchen diesen Eigenwert zu berechnen."
        )
        self.play(Write(eqr))
        self.next_section("last")
        self.wait()
