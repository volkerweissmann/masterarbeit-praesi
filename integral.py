from common import *


class Integral(MyScene):
    def construct(self):
        eqs = 0.8
        scale_lambda = 0.7

        off1 = np.array([-2, 3.5, 0])
        off2 = np.array([4, 3.5, 0])

        delta1 = np.array([0, -0.7, 0])
        line1 = off1 + delta1 * 0
        line2 = off1 + delta1 * 1
        line3 = off1 + delta1 * 2
        line4 = off1 + delta1 * 3
        line5 = off1 + delta1 * 4
        line6 = off1 + delta1 * 5

        line3r = off2 + delta1 * 2
        line5r = off2 + delta1 * 4

        pos_7_1 = off1 + delta1 * 6 + np.array([-3, 0, 0])
        pos_7_2 = off1 + delta1 * 6 + np.array([+3, 0, 0])

        pos_9_1 = off1 + delta1 * 8 + np.array([-3, 0, 0])
        pos_9_2 = off1 + delta1 * 8 + np.array([+1.5, 0, 0])
        pos_9_3 = off1 + delta1 * 8 + np.array([+6, 0, 0])

        # fpo = MathTex(
        #     r"\hat{\mathcal L} = \left(-F'(q) + \epsilon Q''(q)\right) + \left(-F(q)+2\epsilon Q'(q)\right)\partial_q + \epsilon Q(q)\partial_q^2"
        # ).move_to(line1)
        eveq = MathTex(r"\hat{\mathcal L} v(q)=\lambda v(q)").scale(eqs).move_to(line1)
        dgl = (
            MathTex(
                r"0=\lambda v(q) + \partial_q\left(F(q)v(q)\right) - \epsilon\partial_q^2\left(Q(q)v(q)\right)"
            )
            .scale(eqs)
            .move_to(line3)
        )
        d1 = (
            Tex(r"1D")
            .scale(eqs)
            .move_to(line2 + np.array([0.5, 0, 0]), aligned_edge=LEFT)
        )
        ansatz = (
            MathTex(r"g(q) = -\epsilon v'(q)/v(q)")
            .scale(eqs)
            .move_to(line4 + np.array([3, 0, 0]))
        )
        ar2 = Arrow(line1, line3)
        ode = Tex("Differential equation for $g$").scale(eqs).move_to(line5)
        ar4 = Arrow(line3, line5)
        ar4r = Arrow(line3r, line5r)

        weaknoise = MathTex("\epsilon\ll 1").scale(eqs).move_to(line6)

        g1 = MathTex(r"g_1(q)").move_to(pos_7_1)
        g2 = MathTex(r"g_2(q)").move_to(pos_7_2)

        ar61 = Arrow(line5, pos_7_1, buff=0.5)
        ar62 = Arrow(line5, pos_7_2, buff=0.5)

        perb = MathTex(r"v(P)=v(0)").scale(eqs).move_to(line3r)
        perb2 = (
            MathTex(r"\epsilon i2\pi k = \int_0^P f(g(q))\mathrm dq")
            .scale(eqs)
            .move_to(line5r)
            .shift([0.4, 0, 0])
        )
        # lambda_2 = (
        #     MathTex(
        #         r"\Re\lambda =-\frac 1\epsilon\int_0^P \frac{F(q)}{Q(q)}\mathrm dq\, T "
        #     )
        #     .scale(scale_lambda)
        #     .move_to(pos_9_1)
        # )
        # lambda_prime = (
        #     MathTex(
        #         r"\Re\lambda = -\frac 1\epsilon T\left(\int_0^P\frac{Q(q)} {F^3(q)}\right)^{-1}"
        #     )
        #     .scale(scale_lambda)
        #     .move_to(pos_9_2)
        # )
        # lambda_main = (
        #     MathTex(
        #         r"\Re\lambda = -\epsilon \frac{(2\pi k)^2}{T^3}\int_0^P \frac{Q(q)}{F^3(q)}\mathrm{d}q"
        #     )
        #     .scale(scale_lambda)
        #     .move_to(pos_9_3)
        # )
        lambda_2 = (
            MathTex(r"\lambda =-\frac {c_1}\epsilon  + i k c_2 ")
            .scale(eqs)
            .move_to(pos_9_1)
        )
        lambda_prime = (
            MathTex(r"\lambda = -\frac{c_3}\epsilon - i k c_4")
            .scale(eqs)
            .move_to(pos_9_2)
        )
        lambda_main = (
            MathTex(r"\lambda = -\epsilon k^2 c_5 + i k c_6")
            .scale(eqs)
            .move_to(pos_9_3)
        )
        # todo sagen, dass für k=0 lambda=0 ist.

        # todo: wir nähern zweimal kleine epsilon
        ar81 = Arrow(pos_7_1, pos_9_1, buff=0.4)
        ar82 = Arrow(pos_7_2, pos_9_2, buff=0.4)
        ar83 = Arrow(pos_7_2, pos_9_3, buff=0.4)

        ar581 = Arrow(line5r, pos_9_1, buff=1.1)
        ar582 = Arrow(line5r, pos_9_2 + np.array([0.7, 0, 0]), buff=0.5)
        ar583 = Arrow(line5r, pos_9_3, buff=0.5)

        # ode = (
        #     MathTex(
        #         r"0=\left(Q(q)g^2(q) + F(q)g(q)\right) - \epsilon\left(Q(q)g'(q)+2Q'(q)g(q)+F'(q)+\lambda\right)+\epsilon^2Q''(q)"
        #     )
        #     .scale(0.7)
        #     .move_to(line1)
        # )

        self.subtitle(
            "Im allgemeinen Fall kann ich nicht die Eigenwerte des Fokker-Planck Operators bestimmen. Aber ich kann es, wenn wir zwei Einschränkungen vornehmen. Zum einen betrachten wir jetzt nur eindimensionale Systeme da ist der Fokker-Planck Operator das hier. Zum anderen betrachten wir nur kleine epsilon, also den weak-noise limit."
        )
        self.play(Write(eveq))
        self.next_section()
        self.play(Create(ar2), Write(d1))
        self.play(Write(dgl))
        self.next_section()
        self.play(Write(perb))
        self.next_section()
        self.play(Write(ansatz))
        self.next_section()
        self.play(Create(ar4))
        self.play(Write(ode))
        self.next_section()
        self.play(Create(ar4r))
        self.play(Write(perb2))
        self.next_section()
        self.play(Write(weaknoise))
        self.next_section()
        self.play(Create(ar61), Create(ar62))
        self.play(Write(g1), Write(g2))
        self.next_section()
        self.play(Create(ar81), Create(ar581))
        self.next_section()
        self.play(Write(lambda_2))
        self.next_section("secondlast")
        self.play(
            Create(ar82),
            Create(ar83),
            Create(ar582),
            Create(ar583),
        )
        # self.next_section()
        self.play(Write(lambda_prime))
        # self.next_section()
        self.play(Write(lambda_main))
        self.next_section("last")
        self.wait()
