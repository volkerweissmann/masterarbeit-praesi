from common import *


class MyRes(MyScene):
    def construct(self):
        eqs = 0.7
        # todo sagen, dass für k=0 lambda=0 ist.
        lambdak = (
            MathTex(
                r"\lambda_k &= i",
                r" (2\pi k)\left(\int_0^P\frac{1}{F(q)}\mathrm dq\right)^{-1}",
                r"\\ &-",
                r"\epsilon(2\pi k)^2\left(\int_0^P\frac{1}{F(q)}\mathrm dq\right)^{-3}\left(\int_0^P\frac{Q(q)}{F^3(q)}\mathrm dq\right)",
            )
            .scale(eqs)
            .move_to([0, 2.5, 0])
        )
        # lambda1 = MathTex(
        #     r"\lambda_{+1} &= i(2\pi)\left(\int_0^P\frac{1}{F(q)}\mathrm dq\right)^{-1} \\ &-\epsilon (2\pi) ^2\left(\int_0^P\frac{1}{F(q)}\mathrm dq\right)^{-3}\left(\int_0^P\frac{Q(q)}{F^3(q)}\mathrm dq\right)"
        # )
        #
        gamma = (
            MathTex(
                r"\gamma = -\Re\lambda_{+1} = -\Re\lambda_{-1} =",
                r" \epsilon(2\pi)^2\left(\int_0^P\frac{1}{F(q)}\mathrm dq\right)^{-3}\left(\int_0^P\frac{Q(q)}{F^3(q)}\mathrm dq\right)",
            )
            .scale(eqs)
            .move_to([0, 0, 0])
        )
        omega = (
            MathTex(
                r"\omega = \Im\lambda_{+1} = -\Im\lambda_{-1} =",
                r"(2\pi)\left(\int_0^P\frac{1}{F(q)}\mathrm dq\right)^{-1}",
            )
            .scale(eqs)
            .move_to([0, -1.5, 0])
        )
        R = MathTex(r"R = \frac\omega\gamma").scale(eqs).move_to([0, -3, 0])

        # self.play(Write(lambdak))
        # self.play(Write(gamma), Write(omega))
        # self.play(Write(R))
        self.add(lambdak, gamma, omega, R)
        # self.play(Transform(lambdak, VGroup(gamma, omega)))
        # self.play(Transform(lambdak[3].copy(), gamma))
        # self.play(Transform(lambdak[1].copy(), omega))
        self.wait()
        self.next_section("last")

        self.remove(lambdak, gamma, omega, R)
        self.add(
            MathTex(
                r"\partial_t p(q,t) = -\partial_q\left(F(q)p(q,t)\right) + \epsilon\partial_q^2\left(Q(q)p(q,t)\right)"
            ).move_to([0, 3, 0])
        )
        self.add(
            Tex(
                r"Die Robustness eines eindimensionalen Langevin-Systems\\mit periodischen Randbedingungen welches durch die\\obrige Fokker-Planck Gleichung beschrieben wird\\ist für kleine $\epsilon$"
            ).move_to([0, 1, 0])
        )
        self.add(
            MathTex(
                r"R = \frac{1}{\epsilon(2\pi)}\left(\int_0^P\frac{1}{F(q)}\mathrm dq\right)^{2} \left(\int_0^P\frac{Q(q)}{F^3(q)}\mathrm dq\right)^{-1} "
            ).move_to([0, -2, 0])
        )
        self.wait()
        # self.add("")
